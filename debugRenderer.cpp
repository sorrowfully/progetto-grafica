#include <glm/glm.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include "debugRenderer.h"
#include "shader.h"
#include "engine.h"

DebugRenderer::DebugRenderer(Engine* rengine) {
    engine = rengine;
    camera = rengine->getCamera();
    shader = new DebugShader();
    cube = new Model("../resources/objects/cube/cube.obj", false);
}

void DebugRenderer::renderCube(glm::vec3 position, glm::vec3 color) {
    shader->use();
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, position);
    model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
    glm::mat4 projection = glm::perspective(glm::radians(camera->Zoom), engine->getRenderer()->GetScreenRatio(), 0.1f, 100.0f);
    glm::mat4 view = camera->GetViewMatrix();
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);
    shader->setMat4("model", model);
    shader->setVec3("cubeColor", color);
    cube->Draw(shader);
}

#ifndef PROGETTOGRAFICA_LOGGER_H
#define PROGETTOGRAFICA_LOGGER_H
#include <string>
#include <iostream>
using namespace std;
class Logger {
public:
    const static int LOG_OFF = -1;
    const static int LOG_VERBOSE = 1;
    const static int LOG_DEBUG = 2;
    const static int LOG_ERROR = 3;
    static void verbose(const string &message);
    static void debug(const string &message);
    static void error(const string &message);
    static void setLogLevel(int level);
private:
    static int loglevel;
};
#endif //PROGETTOGRAFICA_LOGGER_H

#include <glm/ext/matrix_transform.hpp>
#include "transform.h"

glm::mat4 &Transform::GetTransform() {
    return transform;
}

void Transform::Translate(float x, float y, float z) {
    transform = glm::translate(transform, glm::vec3(x,y,z));
}

void Transform::Translate(glm::vec3 translation) {
    transform = glm::translate(transform, translation);
}

void Transform::Scale(float x, float y, float z) {
    transform = glm::scale(transform, glm::vec3(x,y,z));
}

void Transform::Rotate(float angle, float x, float y, float z) {
    transform = glm::rotate(transform, angle, glm::vec3(x,y,z));
}

void Transform::Scale(float amount) {
    Scale(amount, amount, amount);
}



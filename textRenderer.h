#ifndef PROGETTOGRAFICA_TEXTRENDERER_H
#define PROGETTOGRAFICA_TEXTRENDERER_H

#include <glad/glad.h>
#include <glm/vec2.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#include "shader.h"
#include "engine.h"

struct Character {
    GLuint TextureID;   // ID handle of the glyph texture
    glm::ivec2 Size;    // Size of glyph
    glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
    GLuint Advance;    // Horizontal offset to advance to next glyph
};


class TextRenderer {
public:
    TextRenderer(Engine* engine);
    void LoadLibrary();
    void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
    void RenderText(float number, string label, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
    void RenderText(glm::vec3 vec, string label, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
private:
    Engine* m_engine;
    TextShader* textShader;
    std::map<GLchar, Character> Characters;
    GLuint VAO, VBO;
};


#endif //PROGETTOGRAFICA_TEXTRENDERER_H

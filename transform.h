#ifndef PROGETTOGRAFICA_TRANSFORM_H
#define PROGETTOGRAFICA_TRANSFORM_H

#include <glm/glm.hpp>

class Transform {
public:
    glm::mat4 &GetTransform();
    void Translate(float x, float y, float z);
    void Translate(glm::vec3 translation);
    void Scale(float x, float y, float z);
    void Scale(float amount);
    void Rotate(float angle, float x, float y, float z);

    inline glm::vec3 GetPosition() { return glm::vec3(transform[3]); }
private:
    glm::mat4 transform = glm::mat4(1.0f);
};


#endif //PROGETTOGRAFICA_TRANSFORM_H

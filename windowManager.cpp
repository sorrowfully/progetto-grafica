#include "windowManager.h"

void frame_buffer_size_callback(GLFWwindow *window, int width, int height);
void input_callback(GLFWwindow* window);
void mouse_callback(GLFWwindow* rwindow, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* rwindow, int button, int action, int mods);
void scroll_callback(GLFWwindow* rwindow, double xoffset, double yoffset);

Camera* m_camera;
Engine* m_engine;
WindowManager* self;

float movespeed = 5.0f;
float rotatespeed = 5.0f;

WindowManager::WindowManager(Engine *engine, Camera *rcamera) {
    self = this;
    assert(engine != NULL);
    assert(rcamera != NULL);
    this->engine = engine;
    m_engine = this->engine;
    this->camera = rcamera;
    m_camera = rcamera;
}

void WindowManager::InitWindow(int width, int height) {
    m_width = width;
    m_height = height;

    lastX = m_width / 2.0f;
    lastY = m_height / 2.0f;
    firstMouse = true;

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Progetto Grafica", NULL, NULL);
    if (window == NULL) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        exit(-1);
    }

    glfwSetFramebufferSizeCallback(window, frame_buffer_size_callback); // callback sul cambiamento delle dimensioni della window
    glfwSetCursorPosCallback(window, mouse_callback); // callback movimento mouse come visuale della camera
    glfwSetMouseButtonCallback(window, mouse_button_callback); // callback per scroll mouse -> zoom sulla camera
    glfwSetScrollCallback(window, scroll_callback); // callback per scroll mouse -> zoom sulla camera
    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // cattura il mouse

    glfwMakeContextCurrent(window);
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        exit(-1);
    }
}

void frame_buffer_size_callback(GLFWwindow *window, int width, int height) {
    Logger::debug("Window callback : frame size changed ("+ std::to_string(width) + "," + std::to_string(height) + ")");
    self->setSize(width, height);
    glViewport(0, 0, width, height);
}
void WindowManager::updateInput() {
    double deltaTime = m_engine->getDeltaTime()*2.0f;
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        glfwSetCursorPosCallback(window, nullptr);
    }

    // sposta la posizione della camera
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        m_camera->ProcessKeyboard(FORWARD, deltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        m_camera->ProcessKeyboard(BACKWARD, deltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        m_camera->ProcessKeyboard(LEFT, deltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        m_camera->ProcessKeyboard(RIGHT, deltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
        m_camera->ProcessKeyboard(DOWN, deltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        m_camera->ProcessKeyboard(UP, deltaTime);
    }

    // sposta un modello
    if (m_engine->getModels() == nullptr || m_engine->getModels()->empty()) return;
    Transform* model = m_engine->getMovableTransform();
    glm::vec3 position = model->GetPosition();
    float translation = movespeed*deltaTime;
    float rotation = rotatespeed*deltaTime;
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        model->Translate(0, 0,translation);
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        model->Translate(0, 0,-translation);
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        model->Translate(translation, 0,0);
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        model->Translate(-translation, 0,0);
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS) {
        model->Translate(0, translation,0);
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS) {
        model->Translate(0, -translation,0);
    }
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
        model->Rotate(rotation, 0.0f, 1.0f, 0.0f);
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
        model->Rotate(-rotation, 0.0f, 1.0f, 0.0f);
    }

    // controlla i parametri delle luci (tutte allo stesso momento)
    float changespeed = 0.1f;
    for (unsigned int i = 0; i < m_engine->getRenderer()->GetLights().size(); i++) {
        if (m_engine->getRenderer()->GetLights().at(i)->type == 2) {
            PointLight *light = static_cast<PointLight *>(m_engine->getRenderer()->GetLights().at(i));
            if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
                light->intensity += changespeed * 5;
            } else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
                light->intensity -=changespeed * 5;
            }

            if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
                light->attenuation.costant +=changespeed;
            } else if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS) {
                light->attenuation.costant -=changespeed;
            }

            if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
                light->attenuation.linear +=changespeed;
            } else if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
                light->attenuation.linear -=changespeed;
            }

            if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
                light->attenuation.exponent +=changespeed;
            } else if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS) {
                light->attenuation.exponent -=changespeed;
            }

            if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
                light->range +=changespeed;
            } else if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS) {
                light->range -=changespeed;
            }

        }
    }

}


void mouse_callback(GLFWwindow *rwindow, double xpos, double ypos) {
    if (self->firstMouse) {
        self->lastX = xpos;
        self->lastY = ypos;
        self->firstMouse = false;
    }
    float xoffset = xpos - self->lastX;
    float yoffset = self->lastY - ypos; // reversed since y-coordinates go from bottom to top
    self->lastX = xpos;
    self->lastY = ypos;
    m_camera->ProcessMouseMovement(xoffset, yoffset);
}
void mouse_button_callback(GLFWwindow *rwindow, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        glfwSetCursorPosCallback(self->getWindow(), mouse_callback);
        glfwSetInputMode(self->getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
}

void scroll_callback(GLFWwindow *rwindow, double xoffset, double yoffset) {
    m_camera->ProcessMouseScroll(yoffset);
}







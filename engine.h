#ifndef PROGETTOGRAFICA_ENGINE_H
#define PROGETTOGRAFICA_ENGINE_H

#include "model.h"
#include "camera.h"
#include "shader.h"
#include "renderer.h"
#include "lighting.h"

class Engine {
public:
    Engine() {}
    void init(int width, int height);
    void update();
    void start();

    Engine* addModel(Model* model);

    inline Camera* getCamera() { return camera; }
    inline Renderer* getRenderer() { return m_renderer; }
    inline double getDeltaTime() { return deltaTime; }
    inline vector<Model*>* getModels() { return &sceneModels; }

    inline void setMovableTransform(Transform* transform) { movableTransform = transform; }
    inline Transform* getMovableTransform() { return movableTransform; }

private:
    Renderer* m_renderer;
    vector<Model*> sceneModels;
    Camera* camera;
    Transform* movableTransform;

    double deltaTime = 0.0f;
    double lastFrame = 0.0f;

};
#endif //PROGETTOGRAFICA_ENGINE_H

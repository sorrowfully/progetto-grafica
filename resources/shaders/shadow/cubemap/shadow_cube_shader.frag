#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;


struct BaseLight {
    vec3 color;
    float intensity;
};

struct Attenuation {
    float constant;
    float linear;
    float exponent;
};

struct PointLight {
    BaseLight base;
    Attenuation atten;
    vec3 position;
    float range;
    bool castShadow;
};

const int MAX_LIGHT_NUMBER = 10;
uniform sampler2D diffuseTexture;
uniform samplerCube depthMap[MAX_LIGHT_NUMBER];

uniform vec3 viewPos;
uniform float far_plane;
uniform bool shadows;

uniform vec3 C_eyePos;
uniform float specularIntensity;
uniform float specularPower;

uniform PointLight lights[MAX_LIGHT_NUMBER];

vec3 gridSamplingDisk[20] = vec3[]
(
vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1),
vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);

vec3 CalcLight(BaseLight base, vec3 direction, vec3 normal, vec3 worldPos) {
    float diffuseFactor = dot(normal, -direction);

    vec3 diffuseColor = vec3(0, 0, 0);
    vec3 specularColor = vec3(0, 0, 0);

    if (diffuseFactor > 0) {
        diffuseColor = base.color * base.intensity * diffuseFactor;

        vec3 directionToEye = normalize(C_eyePos - worldPos);
        //vec3 reflectDirection = normalize(reflect(direction, normal));
        vec3 halfDirection = normalize(directionToEye - direction);

        float specularFactor = dot(halfDirection, normal);
        //float specularFactor = dot(directionToEye, reflectDirection);
        specularFactor = pow(specularFactor, specularPower);

        if (specularFactor > 0) {
            specularColor = base.color * specularIntensity * specularFactor;
        }
    }
    return diffuseColor + specularColor;
}

vec3 CalcPointLight(PointLight pointLight, vec3 normal, vec3 worldPos) {
    if (pointLight.base.intensity <= 0) return vec3(0, 0, 0);
    vec3 lightDirection = worldPos - pointLight.position;
    float distanceToPoint = length(lightDirection);

    if (distanceToPoint > pointLight.range)
    return vec3(0, 0, 0);

    lightDirection = normalize(lightDirection);

    vec3 color = CalcLight(pointLight.base, lightDirection, normal, worldPos);
    //vec4 color = vec4(1,0,0,1);

    float attenuation = pointLight.atten.constant +
    pointLight.atten.linear * distanceToPoint +
    pointLight.atten.exponent * distanceToPoint * distanceToPoint +
    0.0001;

    return color / attenuation;
    //return vec4(lightDirection, 1);
}

float ShadowCalculation(vec3 fragPos, int lightIndex, samplerCube sampler) {

    // get vector between fragment position and light position
    vec3 fragToLight = fragPos - lights[lightIndex].position;
    // use the fragment to light vector to sample from the depth map
    //float closestDepth = texture(depthMap[0], fragToLight).r;
    //float closestDepth = texture(sampler, fragToLight).r;
    // it is currently in linear range between [0,1], let's re-transform it back to original depth value
    //closestDepth *= far_plane;
    // now get current linear depth as the length between the fragment and light position
    float currentDepth = length(fragToLight);
    // test for shadows
    //float bias = 0.3; // we use a much larger bias since depth is now in [near_plane, far_plane] range
    //float shadow = currentDepth -  bias > closestDepth ? 1.0 : 0.0;
    // display closestDepth as debug (to visualize depth cubemap)
    //FragColor = vec4(vec3(closestDepth / far_plane), 1.0);

    float shadow = 0.0;
    float bias = 0.1;
    int samples = 20;
    float viewDistance = length(viewPos - fragPos);
    float diskRadius = (1.0 + (viewDistance / far_plane)) / 25.0;
    for(int i = 0; i < samples; ++i) {
        float closestDepth = texture(sampler, fragToLight + gridSamplingDisk[i] * diskRadius).r;
        closestDepth *= far_plane;   // undo mapping [0;1]
        if(currentDepth - bias > closestDepth)
        shadow += 1.0;
    }
    shadow /= float(samples);

    // display closestDepth as debug (to visualize depth cubemap)
    // FragColor = vec4(vec3(closestDepth / far_plane), 1.0);

    //float intensfactor = lights[lightIndex].base.intensity/5;
    return shadow /** intensfactor*/;

}

float GetLightShadowValue(vec3 fragPos, int lightIndex) {
    if (lights[lightIndex].base.intensity > 0 && lights[lightIndex].castShadow) {
        switch (lightIndex) {
            case 0 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[0]);
            case 1 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[1]);
            case 2 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[2]);
            case 3 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[3]);
            case 4 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[4]);
            case 5 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[5]);
            case 6 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[6]);
            case 7 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[7]);
            case 8 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[8]);
            case 9 : return ShadowCalculation(fs_in.FragPos, lightIndex, depthMap[9]);
        }
    }
    return 0.0f;
}

void main() {

    vec3 color = texture(diffuseTexture, fs_in.TexCoords).rgb;
    vec3 normal = normalize(fs_in.Normal);

    // ambient
    vec3 ambient = 0.3  * color;
    vec3 finalresult = vec3(0.0f);

    for(int i = 0; i < MAX_LIGHT_NUMBER; i++) {

        // calculate shadow
        float currshadow = GetLightShadowValue(fs_in.FragPos, i);
        vec3 light = CalcPointLight(lights[i], normal, fs_in.FragPos);
        finalresult += (light * (1 - currshadow));

    }

    // evita che la luce si mangi anche l'ambient di default (nei punti cechi di una luce che casta ombre)
    if (finalresult.x < 0 || finalresult.y < 0 || finalresult.z < 0) finalresult = vec3(0);

    FragColor = vec4(vec3(ambient + finalresult) * color, 1);

}
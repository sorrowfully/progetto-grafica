#ifndef RENDERUTIL_H
#define RENDERUTIL_H

#include <GLFW/glfw3.h>

class RenderUtil
{
public:
    static void InitGraphics();
    static void EnableBlendText();
    static void DisableBlendText();
protected:
private:
};

#endif

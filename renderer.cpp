#include "shader.h"

#include "engine.h"
#include "logger.h"
#include "renderUtil.h"
#include "renderer.h"

using namespace std;

vector<unsigned int> shadowFBO;
vector<unsigned int> cubeMap;
const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
float near_plane = 1.0f, far_plane = 100.0f;

bool firstFBOdraw = true;

Renderer::Renderer(Engine* engine, int w, int h) {
    assert(engine != NULL && engine->getCamera() != NULL);
    m_engine = engine;
    m_camera = engine->getCamera();
    SetWindowSize(w, h);
    SetShaders();
}

void Renderer::SetWindowSize(int w, int h) {
    m_swidth = w;
    m_sheight = h;
    m_sratio = (float)w/(float)h;
}

void Renderer::BindModels(vector<Model *> *models) {
    m_models = models;
}

void Renderer::SetShaders() {

    shadowShader = new ShadowCubeShader();
    shadowBufferShader = new ShadowDepthCubeShader();
    shadowShader->use();
    shadowShader->setInt("diffuseTexture", 0);
    for (unsigned int i = 0; i < 10; i++) {
        shadowShader->setInt("depthMap["+std::to_string(i)+"]", i + 1);
    }

}

void Renderer::AddPointLight(const glm::vec3 &position, const glm::vec3 &color, float intensity, float range, Attenuation attenuation, bool castShadow) {
    PointLight *l = new PointLight(this, position, color, intensity, range, attenuation);
    l->SetCastShadow(castShadow);
    m_lights.push_back(l);

    shadowFBO.push_back(0);
    cubeMap.push_back(0);

    if (castShadow) {

        cout << "Generating FBO and cubemap texture for light " << m_lights.size() - 1 << endl;

        unsigned int index = shadowFBO.size() - 1;
        unsigned int *FBO = &shadowFBO.at(index);
        unsigned int *MAP = &cubeMap.at(index);

        glGenFramebuffers(1, FBO);
        glGenTextures(1, MAP);
        glBindTexture(GL_TEXTURE_CUBE_MAP, *MAP);
        for (unsigned int j = 0; j < 6; ++j)
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + j, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        // attach depth texture as FBO's depth buffer
        glBindFramebuffer(GL_FRAMEBUFFER, *FBO);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, *MAP, 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

}

void Renderer::Render() {
    updateCubeMaps();
    drawScene();
}

void Renderer::updateCubeMaps() {

    // ottimizzato per disegnare le FBO una sola volta (se le luci non cambiano posizione).
    // togliere se si vuole testare le ombre modificando le posizioni delle luci
    if (!firstFBOdraw) return;
    firstFBOdraw = false;

    // Aggiorna FBO e cubeMaps per le luci PointLight (che proiettano ombre)
    for (unsigned int i = 0; i < m_lights.size(); i++) {
        BaseLight* l = m_lights.at(i);
        if (l->type != BaseLight::POINTLIGHT) continue;
        PointLight *light = static_cast<PointLight *>(l);
        if (!light->castShadow) continue;

        // update light cubemap transforms
        glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT, near_plane, far_plane);

        // light space transform
        std::vector<glm::mat4> shadowTransforms;
        shadowTransforms.push_back(shadowProj * glm::lookAt(light->GetPosition(), light->GetPosition() + glm::vec3( 1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(light->GetPosition(), light->GetPosition() + glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(light->GetPosition(), light->GetPosition() + glm::vec3( 0.0f,  1.0f,  0.0f), glm::vec3(0.0f,  0.0f,  1.0f)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(light->GetPosition(), light->GetPosition() + glm::vec3( 0.0f, -1.0f,  0.0f), glm::vec3(0.0f,  0.0f, -1.0f)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(light->GetPosition(), light->GetPosition() + glm::vec3( 0.0f,  0.0f,  1.0f), glm::vec3(0.0f, -1.0f,  0.0f)));
        shadowTransforms.push_back(shadowProj * glm::lookAt(light->GetPosition(), light->GetPosition() + glm::vec3( 0.0f,  0.0f, -1.0f), glm::vec3(0.0f, -1.0f,  0.0f)));

        // render scene to depth cubemap
        glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
        glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO[i]);
        glClear(GL_DEPTH_BUFFER_BIT);
        shadowBufferShader->use();
        for (unsigned int k = 0; k < 6; ++k)
            shadowBufferShader->setMat4("shadowMatrices[" + std::to_string(k) + "]", shadowTransforms[k]);
        shadowBufferShader->setFloat("far_plane", far_plane);
        shadowBufferShader->setVec3("lightPos", light->GetPosition());

        for (unsigned int j = 0; j < m_models->size(); j++) {
            Model *model = m_models->at(j);
            shadowBufferShader->setMat4("model", model->GetTransform());
            model->Draw(shadowBufferShader);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, m_swidth, m_sheight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}

void Renderer::drawScene() {

    glViewport(0, 0, m_swidth, m_sheight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shadowShader->use();
    glm::mat4 projection = glm::perspective(glm::radians(GetCamera()->Zoom), GetScreenRatio(), 0.1f, 100.0f);
    glm::mat4 view = GetCamera()->GetViewMatrix();

    shadowShader->setMat4("projection", projection);
    shadowShader->setMat4("view", view);
    shadowShader->setVec3("viewPos", GetCamera()->Position);
    shadowShader->setFloat("far_plane", far_plane);

    shadowShader->setVec3("C_eyePos", GetCamera()->Position);
    shadowShader->setFloat("specularIntensity", 3.5f);
    shadowShader->setFloat("specularPower", 10);

    for (unsigned int j = 0; j < m_models->size(); j++) {
        Model *model = m_models->at(j);
        shadowShader->setMat4("model", model->GetTransform());

        for (unsigned int k = 0; k < m_lights.size(); k++) {

            BaseLight *l = m_lights.at(k);
            string lightStruct = "lights["+std::to_string(k)+"]";

            // imposta uniforms per le luci pointlight
            if (l->type == BaseLight::POINTLIGHT) {
                PointLight *light = static_cast<PointLight *>(l);
                shadowShader->setBool(lightStruct + ".castShadow", light->castShadow);
                shadowShader->setVec3(lightStruct + ".position", light->GetPosition());
                shadowShader->setVec3(lightStruct + ".base.color", light->color);
                shadowShader->setFloat(lightStruct + ".base.intensity", light->intensity);
                shadowShader->setFloat(lightStruct + ".range", light->range);
                shadowShader->setFloat(lightStruct + ".atten.constant", light->attenuation.costant);
                shadowShader->setFloat(lightStruct + ".atten.linear", light->attenuation.linear);
                shadowShader->setFloat(lightStruct + ".atten.exponent", light->attenuation.exponent);

                if (light->castShadow) {
                    glActiveTexture(GL_TEXTURE1 + k);
                    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap[k]);
                }
            }
        }
        model->Draw(shadowShader);

    }
    glActiveTexture(GL_TEXTURE0);
}























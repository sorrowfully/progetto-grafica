#include "lighting.h"
#include "renderer.h"

PointLight::PointLight(Renderer* renderer, const glm::vec3 position, const glm::vec3 &color, float intensity, float range, Attenuation attenuation) : BaseLight(POINTLIGHT, renderer, position, color, intensity) {
    assert(GetRenderer() != NULL);
    Translate(position);
    this->attenuation = attenuation;
    this->range = range;
}

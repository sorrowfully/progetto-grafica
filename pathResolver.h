#ifndef PROGETTOGRAFICA_PATHRESOLVER_H
#define PROGETTOGRAFICA_PATHRESOLVER_H
#include <string>
#include <aspell.h>

class PathResolver {
public:
    static std::string ResolveShader(const std::string &filename) {
         const std::string &prefix = std::string("../resources/shaders/");
         return prefix + filename;
    }
};


#endif //PROGETTOGRAFICA_PATHRESOLVER_H

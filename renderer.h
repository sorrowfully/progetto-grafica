#ifndef PROGETTOGRAFICA_RENDERER_H
#define PROGETTOGRAFICA_RENDERER_H

#include "camera.h"
#include "lighting.h"
#include <vector>

class Engine;
class Model;

using namespace std;

class Renderer {
public:
    Renderer (Engine* engine, int screen_width, int screen_height);
    void Render();
    void SetWindowSize(int w, int h);
    void SetShaders();

    void BindModels(vector<Model*> *models);
    void AddPointLight(const glm::vec3 &position, const glm::vec3 &color, float intensity, float range, Attenuation attenuation, bool castruetShadow = false);

    inline Camera* GetCamera() { return m_camera; }
    inline float GetScreenRatio() {return m_sratio; }
    inline vector<BaseLight*> GetLights() { return m_lights; }

private:
    int m_swidth, m_sheight;
    float m_sratio;
    Camera* m_camera;
    Engine* m_engine;
    vector<Model*>* m_models;
    vector<BaseLight*> m_lights;

    Shader* shadowShader;
    Shader* shadowBufferShader;

    void updateCubeMaps();
    void drawScene();

};


#endif //PROGETTOGRAFICA_RENDERER_H

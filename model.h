#ifndef PROGETTOGRAFICA_MODEL_H
#define PROGETTOGRAFICA_MODEL_H


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "mesh.h"
#include "transform.h"

using namespace std;

class Model : public Transform {
public:
    vector<Texture> textures_loaded;
    vector<Mesh*> meshes;
    string directory;
    bool gammaCorrection;
    Model(string const& path, bool gamma);
    void Draw(Shader* shader);

private:
    void loadModel(string const& path);
    void processNode(aiNode* node, const aiScene* scene);
    Mesh* processMesh(aiMesh* mesh, const aiScene* scene);
    vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);
};

unsigned int TextureFromFile(const char* path, const string& directory);

#endif

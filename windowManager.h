#ifndef PROGETTOGRAFICA_WINDOWMANAGER_H
#define PROGETTOGRAFICA_WINDOWMANAGER_H

#include "engine.h"
#include "logger.h"
#include <GLFW/glfw3.h>

class WindowManager {

public:

    float lastX, lastY;
    bool firstMouse;

    WindowManager(Engine *engine, Camera *rcamera);
    void InitWindow(int width, int height);

    void setSize(int width, int height) {
        this->m_width = width;
        this->m_height = height;
    }
    void updateInput();

    inline GLFWwindow* getWindow() { return window; }
    inline Engine* getEngine() {return engine; }
    inline int getWidth() { return m_width; }
    inline int getHeight() { return m_height; }
    inline float getRatio(){ return (float)m_width / (float)m_height; }

private:
    Engine* engine;
    Camera* camera;
    GLFWwindow* window;
    int m_width;
    int m_height;
};

#endif //PROGETTOGRAFICA_WINDOWMANAGER_H

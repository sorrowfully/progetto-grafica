#include "logger.h"
int Logger::loglevel = LOG_DEBUG;
void Logger::verbose(const string &message) {
    if (loglevel >= LOG_VERBOSE)
        cout << "Verbose: " << message << endl;
}
void Logger::debug(const string &message) {
    if (loglevel >= LOG_DEBUG)
        cout << "Debug: " << message << endl;
}
void Logger::error(const string &message) {
    if (loglevel >= LOG_ERROR)
        cout << "Error: " << message << endl;
}
void Logger::setLogLevel(int level) {
    loglevel = level;
}



#ifndef PROGETTOGRAFICA_DEBUGRENDERER_H
#define PROGETTOGRAFICA_DEBUGRENDERER_H

#include <glm/fwd.hpp>

class Model;
class Shader;
class Engine;
class Camera;

class DebugRenderer {
public:
    DebugRenderer(Engine* engine);
    void renderCube(glm::vec3 position, glm::vec3 color = glm::vec3(0.0f, 1.0f, 0.0f));
private:
    Engine* engine;
    Camera* camera;
    Shader* shader;
    Model* cube;


};


#endif //PROGETTOGRAFICA_DEBUGRENDERER_H

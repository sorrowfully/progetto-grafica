#include "engine.h"
#include "pathResolver.h"
#include "logger.h"

#include <ft2build.h>
#include FT_FREETYPE_H

FT_Library  library;

int main() {

    Logger::setLogLevel(Logger::LOG_ERROR);
    Engine engine = Engine();
    engine.init(1500, 950);

    Model* prison = new Model("../resources/objects/prison/prison.obj", false);
    prison->Translate(5,0,-7.0f);
    prison->Scale(0.2f, 0.2f, 0.2f);

    vector<glm::vec3> positions;
    positions.push_back(glm::vec3(-35.74f, 8.78f, 17.26f));
    positions.push_back(glm::vec3(-35.74f, 8.78f, 1.40f));
    positions.push_back(glm::vec3(-35.74f, 8.78f, 32.39f));
    positions.push_back(glm::vec3(-35.74f, 8.78f, -14.39f));
    positions.push_back(glm::vec3(-35.74f, 8.78f, -29.86f));

    for (unsigned int i = 0; i < positions.size(); i++) {
        engine.getRenderer()->AddPointLight(positions.at(i), glm::vec3(0.7f, 0.7f, 0.7f), 24.0f, 55.0f, {1.5,0.02,0.1}, true);
    }

    engine.addModel(prison);
    engine.setMovableTransform(engine.getRenderer()->GetLights().back());
    engine.start();

    return 0;
}

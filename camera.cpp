#include "camera.h"
#include "logger.h"

Camera::Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM) {
    Position = position;
    WorldUp = up;
    Yaw = yaw;
    Pitch = pitch;
    updateCameraVectors();
}

void Camera::updateCameraVectors() {
    // Calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    front.y = sin(glm::radians(Pitch));
    front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    Front = glm::normalize(front);
    // Also re-calculate the Right and Up vector
    Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    Up = glm::normalize(glm::cross(Right, Front));
}

void Camera::updateCameraPosition(glm::vec3 newposition) {
    Position = newposition;
}

glm::mat4 Camera::GetViewMatrix() {
    return glm::lookAt(glm::vec3(Position.x, Position.y, Position.z), Position + Front, Up);
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch) {
    xoffset *= MouseSensitivity;
    yoffset *= MouseSensitivity;

    Yaw += xoffset;
    Pitch += yoffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch) {
        if (Pitch > 89.0f)
            Pitch = 89.0f;
        if (Pitch < -89.0f)
            Pitch = -89.0f;
    }

    // Update Front, Right and Up Vectors using the updated Euler angles
    updateCameraVectors();
}

void Camera::ProcessMouseScroll(float yoffset) {
    if (Zoom >= 1.0f && Zoom <= 45.0f)
        Zoom -= yoffset;
    if (Zoom <= 1.0f)
        Zoom = 1.0f;
    if (Zoom >= 45.0f)
        Zoom = 45.0f;
}

void Camera::ProcessKeyboard(Camera_Movement direction, double deltaTime) {
    float velocity = MovementSpeed * deltaTime;
    if (direction == FORWARD) {
        glm::vec3 newposition = Position;
        newposition += Front * velocity;
        updateCameraPosition(newposition);
    }

    if (direction == BACKWARD) {
        glm::vec3 newposition = Position;
        newposition -= Front * velocity;
        updateCameraPosition(newposition);
    }

    if (direction == LEFT) {
        glm::vec3 newposition = Position;
        newposition -= Right * velocity;
        updateCameraPosition(newposition);
    }

    if (direction == RIGHT) {
        glm::vec3 newposition = Position;
        newposition += Right * velocity;
        updateCameraPosition(newposition);
    }

    if (direction == DOWN) {
        glm::vec3 newposition = Position;
        newposition -= Up * velocity;
        updateCameraPosition(newposition);
    }

    if (direction == UP) {
        glm::vec3 newposition = Position;
        newposition += Up * velocity;
        updateCameraPosition(newposition);
    }
}

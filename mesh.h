#ifndef PROGETTOGRAFICA_MESH_H
#define PROGETTOGRAFICA_MESH_H
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include "shader.h"

using namespace std;

struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
    glm::vec3 Tangent;
    glm::vec3 Bitangent;
};

struct Texture {
    unsigned int id;
    string type;
    string path;
};

class Mesh {
public:
    unsigned int VAO;
    Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures);
    void Draw(Shader* shader);
    vector<Vertex> vertices;
    vector<unsigned int> indices;
    vector<Texture> textures;


private:
    unsigned int VBO, EBO;
    void setupMesh();
};
#endif //PROGETTOGRAFICA_MESH_H

#ifndef PROGETTOGRAFICA_LIGHTING_H
#define PROGETTOGRAFICA_LIGHTING_H
#include <glm/glm.hpp>
#include "transform.h"

class Renderer;
class Shader;

class BaseLight : public Transform {
public:

    static const int DIRECTIONAL = 1;
    static const int POINTLIGHT = 2;

    unsigned int type;
    glm::vec3 color;
    glm::vec3 position;
    float intensity;

    explicit BaseLight(unsigned int type, Renderer* renderer, const glm::vec3 position, const glm::vec3 &color = glm::vec3(0, 0, 0), float intensity = 0) :
        type(type),
        renderer(renderer),
        position(position),
        color(color),
        intensity(intensity)
        {}

    inline Renderer* GetRenderer() { return renderer; }

private:
    Renderer* renderer;
};

struct Attenuation {
    float costant;
    float linear;
    float exponent;
};

class PointLight : public BaseLight {
public:
    bool castShadow = false;
    Attenuation attenuation;
    float range;
    PointLight(Renderer* renderer, const glm::vec3 position, const glm::vec3 &color = glm::vec3(0.0f), float intensity = 0, float range = 0, Attenuation attenuation = {0,0,0});
    inline void SetCastShadow(bool cast) { castShadow = cast; }
};

// Implementazione di altre tipologie di luci qui (non fa parte dell'obiettivo del progetto)

#endif //PROGETTOGRAFICA_LIGHTING_H

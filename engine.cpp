#include "engine.h"
#include "renderUtil.h"
#include "camera.h"
#include "logger.h"
#include "windowManager.h"
#include "renderer.h"
#include "textRenderer.h"
#include "debugRenderer.h"

using namespace std;

WindowManager* windowManager;
TextRenderer* textRenderer;
DebugRenderer* debugRenderer;

int nbFrames = 0;
int lastFPS = 0;
double lastfpstime = 0;
double lastupdatetime = 0;

void Engine::init(int width, int height) {
    camera = new Camera(glm::vec3(-58.96f, 5.92f, 50.49f));

    windowManager = new WindowManager(this, camera);
    windowManager->InitWindow(width,height);

    textRenderer = new TextRenderer(this);
    debugRenderer = new DebugRenderer(this);
    m_renderer = new Renderer(this, width, height);
    m_renderer->BindModels(&sceneModels);

    RenderUtil::InitGraphics();
}

void Engine::start() {
    lastfpstime = glfwGetTime();
    while (!glfwWindowShouldClose(windowManager->getWindow())) {

        glfwPollEvents();
        windowManager->updateInput();

        double now = glfwGetTime();
        deltaTime = now - lastupdatetime;
        if (now - lastFrame >= 1.0f / 80.0f) {
            update();
            lastFrame = now;
            nbFrames++;
        }

        glfwSwapBuffers(windowManager->getWindow());
        lastupdatetime = now;

        if (now - lastfpstime > 1.0f) {
            lastFPS = nbFrames;
            nbFrames = 0;
            lastfpstime = now;
        }

    }
}

void Engine::update() {

    m_renderer->Render();

    for (unsigned int i = 0; i < m_renderer->GetLights().size(); i++) {
        BaseLight *light = m_renderer->GetLights()[i];
        debugRenderer->renderCube(light->GetPosition());
        textRenderer->RenderText(light->GetPosition(), "Light " + std::to_string(i), 10, 770 - (i * 15), 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    }

    PointLight *light = static_cast<PointLight*>(m_renderer->GetLights().at(0));
    textRenderer->RenderText(light->intensity, "light intensity", 15, 105, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(light->attenuation.costant, "light attenuation.costant", 15, 90, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(light->attenuation.linear, "light attenuation.linear", 15, 75, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(light->attenuation.exponent, "light attenuation.exponent", 15, 60, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(light->range, "light range", 15, 120, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(camera->Position, "camera", 15, 15, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(sceneModels.at(0)->GetPosition(), "model", 15, 30, 0.5f, glm::vec3(1.0f,1.0f,1.0f));
    textRenderer->RenderText(lastFPS, "FPS", 1100, 770, 0.5f, glm::vec3(1.0f,1.0f,1.0f));

}

Engine* Engine::addModel(Model* model) {
    cout << "Adding model to scene" << endl;
    sceneModels.push_back(model);
    return this;
}
















